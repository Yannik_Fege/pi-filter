#!/bin/bash
# Run Script

set -e
cleanup() {
    kill -9 $P1
    kill -9 $P2

    sudo service mosquitto stop
    sudo rm /var/lib/mosquitto/mosquitto.db
    sudo service mosquitto start
}
trap cleanup EXIT

/home/pi/Programs/Filter/Build/Filter.out hifiberry &
PX=$!
kill -9 $PX

#./Build/Filter.out hifiberry &
/home/pi/Programs/Filter/Build/Filter.out hifiberry &
P1=$!
 octave /home/pi/Programs/Filter/Autostart/autooct.m &
P2=$!

while true ; do
   echo " " ; sleep 300
   done