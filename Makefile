FILTERIN := ./Source/Filter.c
CONTROLLERIN := ./Source/Controller.c

FILTEROUT := ./Build/Filter.out
CONTROLLEROUT := ./Build/Controller.mex

OCTAVE := /usr/include/octave-4.4.1
LIBS := -lasound -lm -lpaho-mqtt3c -lcjson -I/Source/include -I$(OCTAVE)

OUT_DIR := ./Build

all: ${OUT_DIR} $(FILTEROUT) $(CONTROLLEROUT)

${OUT_DIR}:
	mkdir ${OUT_DIR}

$(FILTEROUT): $(FILTERIN)
	gcc -g $(FILTERIN) -o $(FILTEROUT) $(LIBS)

$(CONTROLLEROUT): $(CONTROLLERIN)
	octave ./mkoct.m