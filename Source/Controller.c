/*
 ============================================================================
 Name        : Controller.c
 Author      : Yannik Fege
 Version     : 0.6
 Copyright   : Your copyright notice
 Description : Calculates coefficients to be used by filter
 ============================================================================
 */

// Include Header
#include "include/Controller.h"

/*
 *	Calculates the coefficients required for a biquad filter and posts them to the broker
 *	@void
*/
void calcBiquad(void) {

	// Calculate Coefficients
	mxArray *in[3], *butterOut[3], *zp2sosOut[1], *Wn;

	in[0] = mxCreateDoubleScalar(n);
	Wn = mxCreateDoubleMatrix(1,2,mxREAL);
	double *vWn = mxGetPr(Wn);
	vWn[0] = lWn;
	vWn[1] = hWn;

	switch (type) {
	case lowpass:
		in[1] = mxCreateDoubleScalar(lWn);
		in[2] = mxCreateString("low");
		break;

	case highpass:
		in[1] = mxCreateDoubleScalar(lWn);
		in[2] = mxCreateString("high");
		break;

	case bandpass:
		in[1] = Wn;
		in[2] = mxCreateString("bandpass");
		break;

	case bandstop:
		in[1] = Wn;
		in[2] = mxCreateString("stop");
		break;
	}

    mexCallMATLAB(3, butterOut, 3, in, "butter");
	mexCallMATLAB(1, zp2sosOut, 3, butterOut, "zp2sos");

	double *Values = mxGetPr(zp2sosOut[0]);
	int dimM = mxGetM(zp2sosOut[0]);
	printf("%d\n", dimM);

	struct cJSON *varCoefficients = cJSON_CreateObject();
	struct cJSON *biquads = cJSON_AddArrayToObject(varCoefficients, "biquads");
	for(int i = 0; i < dimM; i++) {
		char x[250] = { 0 };
		sprintf(
			x,
			"{\"a0\":\"%.16f\",\"a1\":\"%.16f\",\"a2\":\"%.16f\",\"b0\":\"%.16f\",\"b1\":\"%.16f\",\"b2\":\"%.16f\"}",
			Values[(int)(i+0*dimM)], 
			Values[(int)(i+1*dimM)], 
			Values[(int)(i+2*dimM)], 
			Values[(int)(i+3*dimM)], 
			Values[(int)(i+4*dimM)], 
			Values[(int)(i+5*dimM)]
		);
		struct cJSON *bq = cJSON_Parse(x);
		cJSON_AddItemToArray(biquads, bq);
	}
	char sRows[5] = { 0 };
	sprintf(sRows, "%d", dimM);
	struct cJSON *rows = cJSON_CreateString(sRows);
	cJSON_AddItemToObject(varCoefficients, "rows", rows);
	char *toSend = cJSON_Print(varCoefficients);

	mexCallMATLAB(0,NULL,1,zp2sosOut,"disp");

	MQTTClient_deliveryToken token;
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	pubmsg.payload = toSend;
	pubmsg.payloadlen = strlen(toSend);
	pubmsg.qos = QOS;
	pubmsg.retained = 1;

	// Publish Data
	MQTTClient_publishMessage(writer, "/coef", &pubmsg, &token);

	// Clean up
	mxDestroyArray(in[0]);
	mxDestroyArray(in[1]);
	mxDestroyArray(in[2]);

	mxDestroyArray(butterOut[0]);
	mxDestroyArray(butterOut[1]);
	mxDestroyArray(butterOut[2]);

	mxDestroyArray(zp2sosOut[0]);

	free(toSend);
	cJSON_Delete(varCoefficients);
	// ----

	return;
}

// Callbacks

/*
 *	Confirms delivery of message to the MQTT broker
 *	@*context		not used
 *	@dt				identification of message send
*/
void delivered(void *context, MQTTClient_deliveryToken dt)
{
	printf("Message with token value %d delivery confirmed\n", dt);
	deliveredtoken = dt;
}
/*
 *	Handles an incoming Publish event
 *	@*context		not used
 *	@*topicName		Name of topic that the event was triggered by
 *	@topicLen		Length of topicName string
 *	@*message		Contains message aswell as additional information
*/
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	char* payloadptr = message->payload;

	if (!strcmp(topicName, "/InputProperties")) {

		char* payloadptr = message->payload;

		struct cJSON* message = cJSON_Parse(payloadptr);

		if(message != NULL){
			type = cJSON_GetObjectItem(message, "Type")->valueint;
			lWn = (cJSON_GetObjectItem(message, "lWn")->valuedouble*2) / SampleRate;
			hWn = (cJSON_GetObjectItem(message, "hWn")->valuedouble*2) / SampleRate;
			n = cJSON_GetObjectItem(message, "n")->valueint;
			printf("Recieved InputProperties %d, %f, %f, %d", type, lWn, hWn, n);
			putchar('\n');
		}
		calcBiquad();
	}

	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}
/*
 *	Notifies about a lost connection
 *	@*context		not used
 *	@*cause			specifies the cause of the disconnect
*/
void connlost(void *context, char *cause)
{
	printf("\nConnection lost\n");
	printf("     cause: %s\n", cause);
}

// ----

/*
 *	Initialises a Client and creates a connection to an MQTT broker
 *	@*client			Pointer to client that needs to be set
 *	@IPAddress[]		string containing an IP address to the device running the MQTT broker
 *	@*ClientID			Pointer to Client identifier string
 *	@persistence		defines if the value should be stored in broker
 *	@keepAliveInterval	defines how long the client should wait for confirmation from the broker
 *	@cleansession		?
 *	@reader				defines if the client is a reader or a writer
*/
void Init_Client(MQTTClient *client, char IPAddress[], char *ClientID, int persistence, int keepAliveInterval, int cleansession, bool isReader) {
	MQTTClient_connectOptions options = MQTTClient_connectOptions_initializer;
	options.keepAliveInterval = keepAliveInterval;
	options.cleansession = cleansession;

	MQTTClient_create(client, IPAddress, ClientID, persistence, NULL);

	if (isReader) {
		MQTTClient_setCallbacks(*client, NULL, connlost, msgarrvd, delivered);
	}

	if (MQTTClient_connect(*client, &options) != MQTTCLIENT_SUCCESS) {
		exit(0);
	}
}

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	SampleRate = 48000;

	Init_Client(&reader, ADDRESS, CLIENTID2, MQTTCLIENT_PERSISTENCE_NONE, 20, 1, true);
	Init_Client(&writer, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, 20, 1, false);

	// Set Defaults
	MQTTClient_deliveryToken token;
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	pubmsg.payload = "{\"Type\": 0, \"lWn\": 10000, \"hWn\": 10000, \"n\": 3}";
	pubmsg.payloadlen = strlen(pubmsg.payload);
	pubmsg.qos = QOS;
	pubmsg.retained = 1;
	MQTTClient_publishMessage(writer, "/InputProperties", &pubmsg, &token);

	MQTTClient_deliveryToken token2;
	MQTTClient_message pubmsg2 = MQTTClient_message_initializer;
	pubmsg2.payload = "0";
	pubmsg2.payloadlen = strlen(pubmsg2.payload);
	pubmsg2.qos = QOS;
	pubmsg2.retained = 1;
	MQTTClient_publishMessage(writer, "/enabled", &pubmsg2, &token2);
	// -----

	MQTTClient_subscribe(reader, "/InputProperties", QOS);

	char ch;
	do
	{
		ch = getchar();
	} while (ch != 'Q' && ch != 'q');

	MQTTClient_disconnect(writer, 10000);
	MQTTClient_destroy(&writer);

	MQTTClient_disconnect(reader, 10000);
	MQTTClient_destroy(&reader);
}
