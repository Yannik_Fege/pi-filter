/*
 ============================================================================
 Name        : Filter.c
 Author      : Yannik Fege
 Version     : 0.8
 Copyright   : Your copyright notice
 Description : Filters analog signals
 ============================================================================
 */

// Include Header
#include "include/Filter.h"

/*
 *	Applies the filter to the values its passed
 *	@in			value to apply filter to
*/
float process(float in) {
	float out = 0;
	for(int i = 0; i < Coef.rows; i++) {
		out = in * Coef.biquads[i].a0 + Env.envs[i].z1;
    	Env.envs[i].z1 = in * Coef.biquads[i].a1 + Env.envs[i].z2 - Coef.biquads[i].b1 * out;
    	Env.envs[i].z2 = in * Coef.biquads[i].a2 - Coef.biquads[i].b2 * out;
		in = out;
	}
	return out;
}

/*
 *	Configures the audio device passed to the programm
 *	@*device		pointer to the device to use
 *	@channels		number of analog streams  coming from the device
*/
int configure_alsa_audio(snd_pcm_t *device, int channels) {

	snd_pcm_hw_params_t *hw_params;
	int                 err = 0;
	unsigned int        tmp, tmp2;
	snd_pcm_uframes_t   frames;


	// Allocate memory for hardware parameter structure
	if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
		fprintf(stderr, "cannot allocate parameter structure (%s)\n",
			snd_strerror(err));
		return 1;
	}

	// Fill structure from current audio parameters
	tmp2 = snd_pcm_hw_params_any(device, hw_params);
	err = tmp2 < err ? tmp2 : err;

	// Set access type, sample rate, sample format, channels
	tmp2 = snd_pcm_hw_params_set_access(device, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
	err = tmp2 < err ? tmp2 : err;

	// Set resolution to 32 bits
	tmp2 = snd_pcm_hw_params_set_format(device, hw_params, SND_PCM_FORMAT_S32_LE);
	err = tmp2 < err ? tmp2 : err;

	// Set sample rate
	tmp = sample_rate;
	tmp2 = snd_pcm_hw_params_set_rate_near(device, hw_params, &tmp, 0);
	err = tmp2 < err ? tmp2 : err;
	if (tmp != sample_rate) {
		fprintf(stderr, "Could not set requested sample rate, asked for %d got %d\n", sample_rate, tmp);
		sample_rate = tmp;
	}

	// Set out channel
	tmp2 = snd_pcm_hw_params_set_channels(device, hw_params, channels);
	err = tmp2 < err ? tmp2 : err;

	// Set fragment setting
	tmp2 = snd_pcm_hw_params_set_periods_near(device, hw_params, &fragments, 0);
	err = tmp2 < err ? tmp2 : err;

	// Set buffer Size
	int frame_size = channels * (bits / 8);
	frames = buff_size / frame_size * fragments;
	tmp2 = snd_pcm_hw_params_set_buffer_size_near(device, hw_params, &frames);
	err = tmp2 < err ? tmp2 : err;
	if (buff_size != frames * frame_size / fragments) {
		fprintf(stderr, "Could not set requested buffer size, asked for %d got %ld\n", buff_size, frames * frame_size / fragments);
		buff_size = frames * frame_size / fragments;
	}

	// Set HardWare parameters
	tmp2 = snd_pcm_hw_params(device, hw_params);
	err = tmp2 < err ? tmp2 : err;

	if (err < 0) {
		fprintf(stderr, "An Error occured during device configuration\n");
		return 1;
	}
	else {
		return 0;
	}
}

/*
 *	Called on every streambuffer created for value transformation
 *	@rdbu[]			buffer passed to the function
 *	@inframes		buffer length
*/
void do_something(struct chan_buffer rdbuf[], int inframes)
{
	int i;
	int diff;
	time_t rawtime;
	for (i = 0; i < inframes; i++)
	{


		// Mono
		//rdbuf[i].chan1 = (rdbuf[i].chan1 + rdbuf[i].chan2) / 2;

		// Do stuff on chan1
		rdbuf[i].chan1 = process(rdbuf[i].chan1);

		// Mono
		rdbuf[i].chan2 = rdbuf[i].chan1;
	}
}

// Callbacks

/*
 *	Confirms delivery of message to the MQTT broker
 *	@*context		not used
 *	@dt				identification of message send
*/
void delivered(void *context, MQTTClient_deliveryToken dt)
{
	printf("Message with token value %d delivery confirmed\n", dt);
	deliveredtoken = dt;
}
/*
 *	Handles an incoming Publish event
 *	@*context		not used
 *	@*topicName		Name of topic that the event was triggered by
 *	@topicLen		Length of topicName string
 *	@*message		Contains message aswell as additional information
*/
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
	if (!strcmp(topicName, "/enabled")) {
		enabled = atoi(message->payload);
		if (enabled) {
			printf("Filter On");
			putchar('\n');
		}
		else {
			printf("Filter Off");
			putchar('\n');
		}
	}
	if (!strcmp(topicName, "/coef")) {
		printcoefs(Coef, Env);
		char* payloadptr = message->payload;
		char *pEnd;

		struct cJSON* message = cJSON_Parse(payloadptr);
		struct cJSON* recievedBiquads = cJSON_GetObjectItem(message, "biquads");
		sCoef.rows = (int)strtod(cJSON_GetObjectItem(message, "rows")->valuestring, &pEnd);

		if(message != NULL && recievedBiquads != NULL) {

			sCoef.biquads = (struct biquad*) malloc(sCoef.rows * sizeof(struct biquad));
			sEnv.envs = (struct biquadenv*) malloc(sCoef.rows * sizeof(struct biquadenv));

			for(int i = 0; i<sCoef.rows; i++) {
				struct cJSON *tmp = cJSON_GetArrayItem(recievedBiquads, i);

				struct biquad bqToSet;
				bqToSet.a0 = strtod(cJSON_GetObjectItem(tmp, "a0")->valuestring, &pEnd);
				bqToSet.a1 = strtod(cJSON_GetObjectItem(tmp, "a1")->valuestring, &pEnd);
				bqToSet.a2 = strtod(cJSON_GetObjectItem(tmp, "a2")->valuestring, &pEnd);
				bqToSet.b0 = strtod(cJSON_GetObjectItem(tmp, "b0")->valuestring, &pEnd);
				bqToSet.b1 = strtod(cJSON_GetObjectItem(tmp, "b1")->valuestring, &pEnd);
				bqToSet.b2 = strtod(cJSON_GetObjectItem(tmp, "b2")->valuestring, &pEnd);

				sCoef.biquads[i] = bqToSet;

				struct biquadenv envToSet;
				envToSet.z1 = 0;
				envToSet.z2 = 0;
				sEnv.envs[i] = envToSet;
			}
		}
		newCoef = 1;
		cJSON_Delete(message);
		printcoefs(sCoef, sEnv);
	}

	MQTTClient_freeMessage(&message);
	MQTTClient_free(topicName);
	return 1;
}
/*
 *	Notifies about a lost connection
 *	@*context		not used
 *	@*cause			specifies the cause of the disconnect
*/
void connlost(void *context, char *cause)
{
	printf("\nConnection lost\n");
	printf("     cause: %s\n", cause);
}

// ----

/*
 *	Initialises a Client and creates a connection to an MQTT broker
 *	@*client			Pointer to client that needs to be set
 *	@IPAddress[]		string containing an IP address to the device running the MQTT broker
 *	@*ClientID			Pointer to Client identifier string
 *	@persistence		defines if the value should be stored in broker
 *	@keepAliveInterval	defines how long the client should wait for confirmation from the broker
 *	@cleansession		?
 *	@reader				defines if the client is a reader or a writer
*/
void Init_Client(MQTTClient *client, char IPAddress[], char *ClientID, int persistence, int keepAliveInterval, int cleansession, bool isReader) {
	MQTTClient_connectOptions options = MQTTClient_connectOptions_initializer;
	options.keepAliveInterval = keepAliveInterval;
	options.cleansession = cleansession;

	MQTTClient_create(client, IPAddress, ClientID, persistence, NULL);

	if (isReader) {
		MQTTClient_setCallbacks(*client, NULL, connlost, msgarrvd, delivered);
	}

	if (MQTTClient_connect(*client, &options) != MQTTCLIENT_SUCCESS) {
		exit(0);
	}
}

int main(int argc, char *argv[]) {
	int err;
	int i, frames, inframes, outframes, frame_size;

	enabled = 0;


	// Get playback_handle set from ALSA method
	if ((err = snd_pcm_open(&playback_handle, argv[1], SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
		fprintf(stderr, "cannot open output audio device %s: %s\n", snd_device_in, snd_strerror(err));
		exit(1);
	}

	// Get capture_handle set from ALSA method
	if ((err = snd_pcm_open(&capture_handle, argv[1], SND_PCM_STREAM_CAPTURE, 0)) < 0) {
		fprintf(stderr, "cannot open input audio device %s: %s\n", snd_device_out, snd_strerror(err));
		exit(1);
	}

	// Set config parameters for both capture and playback handle
	configure_alsa_audio(capture_handle, nchannels);
	configure_alsa_audio(playback_handle, nchannels);

	Init_Client(&reader, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, 20, 1, true);
	MQTTClient_subscribe(reader, "/coef", QOS);
	MQTTClient_subscribe(reader, "/enabled", QOS);

	restarting = 1;

	while (1)
	{
		if (!newCoef) {
			frame_size = nchannels * (bits / 8);
			frames = buff_size / frame_size;

			if (restarting) {
				restarting = 0;
				/* drop any output we might got and stop */
				snd_pcm_drop(capture_handle);
				snd_pcm_drop(playback_handle);
				/* prepare for use */
				snd_pcm_prepare(capture_handle);
				snd_pcm_prepare(playback_handle);

				/* fill the whole output buffer */
				for (i = 0; i < fragments; i += 1)
					snd_pcm_writei(playback_handle, rdbuf, frames);
			}

			while ((inframes = snd_pcm_readi(capture_handle, rdbuf, frames)) < 0) {
				if (inframes == -EAGAIN)
					continue;
				// by the way, writing to terminal emulator is costly if you use
				// bad emulators like gnome-terminal, so don't do this.
				fprintf(stderr, "Input buffer overrun\n");
				restarting = 1;
				snd_pcm_prepare(capture_handle);
			}
			if (inframes != frames)
				fprintf(stderr, "Short read from capture device: %d, expecting %d\n", inframes, frames);

			/* now processes the frames, if Filter is enabled */
			if (enabled && Coef.biquads != NULL && Env.envs != NULL) {
				do_something(rdbuf, inframes);
			}

			while ((outframes = snd_pcm_writei(playback_handle, rdbuf, inframes)) < 0) {
				if (outframes == -EAGAIN)
					continue;
				fprintf(stderr, "Output buffer underrun\n");
				restarting = 1;
				snd_pcm_prepare(playback_handle);
			}
			if (outframes != inframes)
				fprintf(stderr, "Short write to playback device: %d, expecting %d\n", outframes, frames);
		}
		else {
			if(sCoef.biquads != NULL && sEnv.envs != NULL){
				if(Coef.biquads != NULL && Env.envs != NULL) {

					printf("Freeing old Coefficients");
					putchar('\n');

					free(Coef.biquads);
					free(Env.envs);
				}

				Coef = sCoef;
				Env = sEnv;

				printcoefs(Coef, Env);

				newCoef = 0;
				restarting = 1;
			}
		}
	}

	MQTTClient_disconnect(reader, 10000);
	MQTTClient_destroy(&reader);
	return 1;
}

void printcoefs(struct sos refcoef, struct biquadenvs refenv){
	printf("New Coefficients recieved!");
		putchar('\n');
		for(int i = 0; i < refcoef.rows; i++) {
			printf(
				"Row %d: BQ %f : %f : %f :: %f : %f : %f ENV %f :: %f", 
				i, 
				refcoef.biquads[i].b0,
				refcoef.biquads[i].b1,
				refcoef.biquads[i].b2,
				refcoef.biquads[i].a0,
				refcoef.biquads[i].a1,
				refcoef.biquads[i].a2,
				refenv.envs[i].z1,
				refenv.envs[i].z2
			);
			putchar('\n');
		}
}