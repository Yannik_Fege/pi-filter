/*
 ============================================================================
 Name        : Controller.h
 Author      : Yannik Fege
 Version     : 0.6
 Copyright   : Your copyright notice
 Description : Controller header
 ============================================================================
 */

// ------- Library includes -------
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "math.h"
#include "MQTTClient.h"
#include "cjson/cJSON.h"
#include <octave/octave.h>
#include <octave/mex.h>
// ----- Library includes end -----

// ------- Definitions -------
#define ADDRESS     "tcp://localhost:1883"
#define CLIENTID    "ControllerWriter"
#define QOS         1
#define TIMEOUT     10000L
#define CLIENTID2   "ControllerReader"
#define QOS2        1
#define TIMEOUT2    10000L
#define PRECISION	16
#define ORDER		3
// ----- Definitions end -----

// ------- Struct declaration -------

// ----- Struct declaration end -----

// ------- Enum declaration -------
enum {
	lowpass = 0,	// 0
	highpass,		// 1
	bandpass,		// 2
	bandstop,		// 3
};
// ----- Enum declaration end -----

// ------- Value declaration -------
static MQTTClient   reader, writer;
volatile            MQTTClient_deliveryToken deliveredtoken;
int                 type, n;
double              SampleRate;
double              lWn, hWn;
// ----- Value declaration end -----

// ------- Function declaration -------
void    calcBiquad(void);
void    delivered(void *context, MQTTClient_deliveryToken dt);
int     msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message);
void    connlost(void *context, char *cause);
void    Init_Client(MQTTClient *client, char IPAddress[], char *ClientID, int persistence, int keepAliveInterval, int cleansession, bool isReader);
int     main(int argc, char* argv[]);
// ----- Function declaration end -----
