/*
 ============================================================================
 Name        : Filter.h
 Author      : Yannik Fege
 Version     : 0.8
 Copyright   : Your copyright notice
 Description : Filter header
 ============================================================================
 */

// ------- Library includes -------
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <alsa/asoundlib.h>
#include "MQTTClient.h"
#include "stdbool.h"
#include "cjson/cJSON.h"
// ----- Library includes end -----

// ------- Definitions -------
#define ADDRESS     "tcp://localhost:1883"
#define CLIENTID    "FilterReader"
#define TOPIC       "/coef"
#define QOS         1
#define TIMEOUT     10000L
#define PRECISION   16
#define BUFF_SIZE   (8192)
#define REG_SIZE 	100
// ----- Definitions end -----

// ------- Struct declaration -------
struct chan_buffer
{
	int chan1;
	int chan2;
};
struct biquad
{
	double a0;
	double a1;
	double a2;
	double b0;
	double b1;
	double b2;
};
struct sos
{
	struct biquad *biquads;
	int rows;
};
struct biquadenv
{
	double z1;
	double z2;
};
struct biquadenvs
{
	struct biquadenv *envs;
};
// ----- Struct declaration end -----

// ------- Enum declaration -------

// ----- Enum declaration end -----

// ------- Value declaration -------
int             restarting, newCoef, enabled;
int             buff_size = BUFF_SIZE;
int             nchannels = 2;
int             sample_rate = 48000;
int             bits = 32;
char           *snd_device_in = "plughw:0,0";
char           *snd_device_out = "plughw:0,0";
snd_pcm_t      *playback_handle;
snd_pcm_t      *capture_handle;
unsigned int    fragments = 2;
static          MQTTClient reader;
volatile        MQTTClient_deliveryToken deliveredtoken;
struct          chan_buffer rdbuf[BUFF_SIZE / 8];
struct			sos sCoef, Coef;
struct			biquadenvs sEnv, Env;
// ----- Value declaration end -----

// ------- Function declaration -------
void 	printcoefs(struct sos refcoef, struct biquadenvs refenv);
float 	process(float in);
int     configure_alsa_audio(snd_pcm_t *device, int channels);
void    do_something(struct chan_buffer rdbuf[], int inframes);
void    delivered(void *context, MQTTClient_deliveryToken dt);
int     msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message);
void    connlost(void *context, char *cause);
void    Init_Client(MQTTClient *client, char IPAddress[], char *ClientID, int persistence, int keepAliveInterval, int cleansession, bool isReader);
int     main(int argc, char *argv[]);
// ----- Function declaration end -----
